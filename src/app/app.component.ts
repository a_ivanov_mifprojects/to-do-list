import {Component, OnInit} from '@angular/core';
import {IsAuthenticatedService} from './cards/services/authenticate/is-authenticated/is-authenticated.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  constructor(private isAuthenticated: IsAuthenticatedService,
              private router: Router) {
  }

  ngOnInit() {
    this.checkAuthentication();

  }

  checkAuthentication() {
    this.isAuthenticated.check().subscribe(() => this.router.navigate(['./cards']),
      () => this.router.navigate(['./main']));
  }
}
