import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {FormComponent} from './form/form.component';
import {AuthenticationComponent} from './authentication/authentication.component';
import {ClickOutsideDirective} from './authentication/click-outside.directive';

@NgModule({
  declarations: [FormComponent, AuthenticationComponent, ClickOutsideDirective],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [AuthenticationComponent]
})
export class LoginModule {
}
