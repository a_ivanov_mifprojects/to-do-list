import {Injectable} from '@angular/core';
import {of, throwError} from 'rxjs';
import {UserDB} from '../../cards/services/authenticate/users-db';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  usersDB: UserDB[] = JSON.parse(localStorage.getItem('usersDB'));

  loginUser(login: string, password: string) {
    const user: UserDB = this.findUser(login);
    if (user === null) {
      return throwError('Unknown user');
    }
    if (!this.checkPassword(user.password, password)) {
      return throwError('Wrong password');
    }
    return of(user.id);
  }

  findUser(login: string) {
    return this.usersDB.filter(user => user.login === login)[0] || null;
  }

  checkPassword(userPassword, password: string) {
    return userPassword === password;
  }
}
