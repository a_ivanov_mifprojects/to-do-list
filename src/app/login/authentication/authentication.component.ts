import {Component, OnInit} from '@angular/core';
import {RegisterService} from '../../cards/services/authenticate/register/register.service';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';
import {UserForm} from '../../cards/services/authenticate/userForm';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
  logged = false;
  userLogin: string;
  error: string;
  showLogout = false;
  showLoginForm = false;
  showRegisterForm = false;

  constructor(private registerService: RegisterService,
              private loginService: LoginService,
              private router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('activeUser') !== null) {
      this.logged = true;
      this.userLogin = JSON.parse(localStorage.getItem('activeUser')).login;
    }
  }

  setShowLogout(state: boolean) {
    this.showLogout = state;
  }

  setShowLoginForm(state: boolean) {
    this.showLoginForm = state;
    this.showRegisterForm = false;
  }

  setShowRegisterForm(state: boolean) {
    this.showRegisterForm = state;
    this.showLoginForm = false;
  }

  login(user: UserForm) {
    this.loginService.loginUser(user.login, user.password).subscribe(id => {
        this.logged = true;
        this.userLogin = user.login;
        localStorage.setItem('activeUser', JSON.stringify({login: user.login, id}));
        this.router.navigate(['./cards']);
        this.setShowLogout(false);
        this.error = null;
      },
      error => this.error = error);
  }

  register(user: UserForm) {
    this.registerService.addUser(user.login, user.password).subscribe(userData => {
        this.userLogin = userData.login;
        localStorage.setItem('activeUser', JSON.stringify({login: user.login, id: userData.id}));
        this.logged = true;
        this.router.navigate(['./cards']);
        this.setShowLogout(false);
        this.error = null;
      },
      error => this.error = error);
  }

  logout() {
    localStorage.removeItem('activeUser');
    this.logged = false;
    this.router.navigate(['../']);
  }
}
