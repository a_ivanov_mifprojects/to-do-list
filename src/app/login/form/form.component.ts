import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input() type: string;
  @Input() error: string;
  @Output() user = new EventEmitter();

  inputType: string;
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      login: ['', [Validators.required, this.validateLogin, Validators.minLength(5)]],
      password: ['', [Validators.required, this.validatePassword, Validators.minLength(8)]]
    });
  }

  ngOnInit() {
    this.setInputType();
  }

  setInputType() {
    this.type === 'login' ? this.inputType = 'password' : this.inputType = 'text';
  }

  setUser() {
    this.user.emit(this.form.value);
  }

  validateLogin(control: AbstractControl) {
    if (control.value.match(/[|\\/~.^:,;?!&%$(#)@*+]/g) !== null) {
      return {validLogin: true};
    }
    return null;
  }

  validatePassword(control: AbstractControl) {
    if (control.value.match(/[|\\/~.^:,;?!&%$()#@*+]/g) !== null) {
      return {validPassword: true};
    }
    return null;
  }
}
