import {Component, OnInit} from '@angular/core';
import {Navigation} from '../services/navigation/navigation-interface';
import {ActivatedRoute, Router} from '@angular/router';
import {UpdateNavigationService} from '../services/navigation/update-navigation.service';
import {Card} from '../services/user-cards/user-cards-interface';
import {UserCardsService} from '../services/user-cards/user-cards.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  navigation: Navigation[];
  cards: Card[];

  constructor(private route: ActivatedRoute,
              private userCardsService: UserCardsService,
              private router: Router,
              private updateNavigationService: UpdateNavigationService) {
  }

  ngOnInit() {
    this.getCards();
    this.checkNavigationUpdate();
  }

  getCards() {
    this.route.data.subscribe(cards => {
      this.cards = cards.cards;
      this.setNavigation();
    });
  }

  addCard(cardtName: string) {
    this.userCardsService.addCard(cardtName).subscribe(card => {
        this.setNavigation();
        this.router.navigate([`./cards/${card.id}`]);
      }
    );
  }

  checkNavigationUpdate() {
    this.updateNavigationService.change.subscribe((cards: Card[]) => {
      this.cards = cards;
      this.setNavigation();
    });
  }

  setNavigation() {
    this.navigation = this.cards.map(item => ({
      title: item.title,
      noCompleted: item.noCompleted.length,
      important: item.important.length,
      id: item.id
    }));
  }
}
