import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewElementComponent} from './new-element/new-element.component';
import {TitleComponent} from './card/title/title.component';
import {NavigationComponent} from './navigation/navigation.component';
import {ItemComponent} from './card/item/item.component';
import {StatisticsComponent} from './card/statistics/statistics.component';
import {CardComponent} from './card/card.component';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {CardsComponent} from './cards/cards.component';
import {CardsRoutingModule} from './cards-routing.module';

@NgModule({
  declarations: [
    NewElementComponent,
    TitleComponent,
    NavigationComponent,
    CardsComponent,
    ItemComponent,
    StatisticsComponent,
    CardComponent,
  ],
  imports: [
    CommonModule,
    CardsRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    CardsComponent
  ]
})
export class CardsModule {
}
