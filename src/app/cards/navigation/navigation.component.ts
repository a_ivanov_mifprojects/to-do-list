import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Navigation} from '../services/navigation/navigation-interface';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  @Input() navigation: Navigation[];
  @Output() newCard = new EventEmitter<string>();

  addCard(listName: string) {
    this.newCard.emit(listName);
  }

  getNoCompleted(noCompleted: number) {
    if (noCompleted > 0) {
      return noCompleted;
    }
  }

  getCommonState(bindOption: string) {
    let state = 0;
    this.navigation.forEach(list => state += list[bindOption]);
    if (state > 0) {
      return state;
    }
  }
}
