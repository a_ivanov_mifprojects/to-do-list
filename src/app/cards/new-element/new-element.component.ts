import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-element',
  templateUrl: './new-element.component.html',
  styleUrls: ['./new-element.component.scss']
})
export class NewElementComponent {
  @Input() placeholder: string;
  @Output() newElement = new EventEmitter();
  @ViewChild('newName') newName: ElementRef;

  formGroup: FormGroup;
  inputBorder = 'none';

  constructor(private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
      input: ['', [Validators.required, Validators.minLength(1)]]
    });
  }

  focusToAddElement() {
    this.newName.nativeElement.focus();
  }

  addElement(elementName: string) {
    if (elementName.length < 2) {
      this.inputBorder = '1px ridge red';
      return;
    }
    this.inputBorder = 'none';
    this.newElement.emit(elementName);
    this.formGroup.reset();
  }
}
