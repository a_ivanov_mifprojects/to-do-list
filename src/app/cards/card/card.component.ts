import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Events} from './event-interface';
import {UserCardsService} from '../services/user-cards/user-cards.service';
import {CardItemsService} from '../services/user-cards/card-items/card-items.service';
import {Card, CardItem} from '../services/user-cards/user-cards-interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  card: Card;

  constructor(private route: ActivatedRoute,
              private userCardsService: UserCardsService,
              private cardItemsService: CardItemsService,
              private router: Router) {
  }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
    this.route.data.subscribe(card => {
      this.card = card.card;
    });
  }

  renameCard(cardId: number, event: Events) {
    this.userCardsService.renameCard(cardId, event.title).subscribe(() => {
    });
  }

  deleteCard(cardId: number) {
    this.userCardsService.removeCard(cardId).subscribe(() => {
      this.router.navigate(['']);
    });
  }

  addItem(cardId: number, name: string) {
    this.cardItemsService.addItem(cardId, name).subscribe(() => {
    });
  }

  setNewItemName(cardId: number, event: Events) {
    this.cardItemsService.renameItem(cardId, event.taskId, event.name).subscribe(() => {
    });
  }

  deleteItem(cardId: number, event: Events) {
    this.cardItemsService.removeItem(cardId, event.taskId).subscribe(() => {
    });
  }

  setImportantItem(cardId: number, event: Events) {
    this.cardItemsService.setImportantItem(cardId, event.taskId, event.state).subscribe(() => {
    });
  }

  setCompletedItem(cardId: number, event: Events) {
    this.cardItemsService.setCompleteItem(cardId, event.taskId, event.state).subscribe(() => {
    });
  }
}
