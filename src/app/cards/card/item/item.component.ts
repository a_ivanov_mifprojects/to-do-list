import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CardItem} from '../../services/user-cards/user-cards-interface';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {
  @Input() item: CardItem;
  @Output() important = new EventEmitter();
  @Output() completed = new EventEmitter();
  @Output() rename = new EventEmitter();
  @Output() delete = new EventEmitter();

  setImportant() {
    this.important.emit({taskId: this.item.id, state: !this.item.important});
  }

  setCompleted() {
    this.completed.emit({taskId: this.item.id, state: !this.item.completed});
  }

  setNewName(name: string) {
    this.rename.emit({taskId: this.item.id, name});
  }

  deleteTask() {
    this.delete.emit({taskId: this.item.id});
  }
}
