import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent {
  @Input() title: string;
  @Output() newTitle = new EventEmitter();
  @Output() delete = new EventEmitter();

  inputBorder = 'none';

  setNewTitle(title: string) {
    if (title.length < 2) {
      this.inputBorder = '1px ridge red';
      return;
    }
    this.inputBorder = 'none';
    this.newTitle.emit({title});
  }

  deleteCard() {
    this.delete.emit();
  }
}
