import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Events} from '../event-interface';
import {Card} from '../../services/user-cards/user-cards-interface';
import {CardItemsService} from '../../services/user-cards/card-items/card-items.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  cards: Card[];
  bindList = this.route.routeConfig.path;
  title: string;

  constructor(private route: ActivatedRoute,
              private cardItemService: CardItemsService) {
  }

  ngOnInit() {
    this.getCards();
    this.setTitle();
  }

  getCards() {
    this.route.data.subscribe(cards => {
      this.cards = cards.cards;
    });
  }

  setTitle() {
    this.bindList === 'noCompleted' ? this.title = 'Tasks' : this.title = 'Important';
  }

  getIndex(array: any[], id: number) {
    return array.findIndex(item => item.id === id);
  }

  setNewTaskName(itemId: number, event: Events) {
    this.cardItemService.renameItem(itemId, event.taskId, event.name).subscribe(() => {
      const listIndex = this.getIndex(this.cards, itemId);
      this.cards[listIndex].items[this.getIndex(this.cards[listIndex].items, event.taskId)].name = event.name;
    });
  }

  deleteTask(listId: number, event: Events) {
    this.cardItemService.removeItem(listId, event.taskId).subscribe(() => {
      const listIndex = this.getIndex(this.cards, listId);
      this.cards[listIndex].important = this.cards[listIndex].important.filter(id => id !== event.taskId);
      this.cards[listIndex].noCompleted = this.cards[listIndex].noCompleted.filter(id => id !== event.taskId);
      this.cards[listIndex].items = this.cards[listIndex].items.filter(task => task.id !== event.taskId);
    });
  }

  setImportantTask(listId: number, event: Events) {
    this.cardItemService.setImportantItem(listId, event.taskId, event.state).subscribe(() => {
      const listIndex = this.getIndex(this.cards, listId);
      this.cards[listIndex].important = this.cards[listIndex].important.filter(id => id !== event.taskId);
      this.cards[listIndex].items[this.getIndex(this.cards[listIndex].items, event.taskId)].important = event.state;
    });
  }

  setCompletedTask(listId: number, event: Events) {
    this.cardItemService.setCompleteItem(listId, event.taskId, event.state).subscribe(() => {
      const listIndex = this.getIndex(this.cards, listId);
      this.cards[listIndex].noCompleted = this.cards[listIndex].noCompleted.filter(id => id !== event.taskId);
      this.cards[listIndex].items[this.getIndex(this.cards[listIndex].items, event.taskId)].completed = event.state;
    });
  }

}
