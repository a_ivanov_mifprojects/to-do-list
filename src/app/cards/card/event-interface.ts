export interface Events {
  title: string;
  taskId: number;
  name: string;
  state: boolean;
}
