import {Injectable} from '@angular/core';
import {of, throwError} from 'rxjs';
import {UserDB} from '../users-db';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  usersDB: UserDB[] = JSON.parse(localStorage.getItem('usersDB')) || [];

  addUser(login: string, password: string) {
    if (!this.checkUserLogin(login)) {
      return throwError('Login already in use');
    }
    const user: UserDB = {login, password, id: new Date().getTime()};
    this.usersDB.push(user);
    localStorage.setItem('usersDB', JSON.stringify(this.usersDB));
    this.createUserToDo(user.id);
    return of(user);
  }

  checkUserLogin(login: string) {
    return this.usersDB.filter(user => user.login === login).length < 1;
  }

  createUserToDo(userId: number) {
    const userToDo = {id: userId, cards: []};
    localStorage.setItem(String(userId), JSON.stringify(userToDo));
  }
}
