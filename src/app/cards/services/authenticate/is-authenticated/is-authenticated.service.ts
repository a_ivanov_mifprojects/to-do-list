import {Injectable} from '@angular/core';
import {of, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedService {

  check() {
    if (localStorage.getItem('activeUser') !== null) {
      return of('ok');
    }
    return throwError('no authenticated');
  }
}
