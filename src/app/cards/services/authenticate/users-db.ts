export interface UserDB {
  login: string;
  password: string;
  id: number;
}
