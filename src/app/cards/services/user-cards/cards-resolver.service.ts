import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {UserCardsService} from './user-cards.service';

@Injectable({
  providedIn: 'root'
})
export class CardsResolverService implements Resolve<any> {

  constructor(private userCardsService: UserCardsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userCardsService.getCards();
  }
}
