import {Injectable} from '@angular/core';
import {UpdateNavigationService} from '../navigation/update-navigation.service';
import {of} from 'rxjs';
import {UserCards} from './user-cards-interface';
import {SetCardTitleService} from './card-title/set-card-title.service';
import {IndexService} from './index/index.service';

@Injectable({
  providedIn: 'root'
})
export class UserCardsService {

  userId = JSON.parse(localStorage.getItem('activeUser')).id;
  userCards: UserCards = JSON.parse(localStorage.getItem(this.userId));

  constructor(private updateNavigationService: UpdateNavigationService,
              private setCardsTitle: SetCardTitleService,
              private indexService: IndexService) {
  }

  getCards() {
    this.userId = JSON.parse(localStorage.getItem('activeUser')).id;
    this.userCards = JSON.parse(localStorage.getItem(this.userId));
    return of(this.userCards.cards);
  }

  addCard(title: string) {
    const card = {
      title: this.setCardsTitle.checkTitle(this.userCards.cards, title),
      noCompleted: [],
      items: [],
      id: new Date().getTime(),
      important: []
    };
    this.userCards.cards.push(card);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of(card);
  }

  renameCard(cardId: number, title: string) {
    this.userCards.cards[this.indexService.getIndex(this.userCards.cards, cardId)].title = this.setCardsTitle.checkTitle(this.userCards.cards, title);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }

  removeCard(cardId: number) {
    this.userCards.cards = this.userCards.cards.filter(list => list.id !== cardId);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }
}
