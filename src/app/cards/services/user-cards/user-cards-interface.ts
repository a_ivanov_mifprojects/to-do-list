export interface UserCards {
  id: number;
  cards: Card[];
}

export interface Card {
  title: string;
  noCompleted: number[];
  important: number[];
  id: number;
  items: CardItem[];
}

export interface CardItem {
  name: string;
  completed: boolean;
  important: boolean;
  id: number;
}
