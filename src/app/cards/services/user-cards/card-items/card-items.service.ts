import {Injectable} from '@angular/core';
import {UpdateNavigationService} from '../../navigation/update-navigation.service';
import {of} from 'rxjs';
import {UserCards} from '../user-cards-interface';
import {IndexService} from '../index/index.service';

@Injectable({
  providedIn: 'root'
})
export class CardItemsService {
  userId = JSON.parse(localStorage.getItem('activeUser')).id;
  userCards: UserCards;

  constructor(private updateNavigationService: UpdateNavigationService,
              private indexService: IndexService) {
  }

  getItems(cardId: number) {
    this.userId = JSON.parse(localStorage.getItem('activeUser')).id;
    this.userCards = JSON.parse(localStorage.getItem(this.userId));
    return of(this.userCards.cards[this.indexService.getIndex(this.userCards.cards, cardId)]);
  }

  addItem(cardId: number, name: string) {
    const item = {name, completed: false, id: new Date().getTime(), important: false};
    this.userCards.cards[this.indexService.getIndex(this.userCards.cards, cardId)].items.push(item);
    this.userCards.cards[this.indexService.getIndex(this.userCards.cards, cardId)].noCompleted.push(item.id);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }

  renameItem(cardId: number, taskId: number, name: string) {
    const listIndex = this.indexService.getIndex(this.userCards.cards, cardId);
    this.userCards.cards[listIndex].items[this.indexService.getIndex(this.userCards.cards[listIndex].items, taskId)].name = name;
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }

  removeItem(cardId: number, taskId: number) {
    const listIndex = this.indexService.getIndex(this.userCards.cards, cardId);
    this.userCards.cards[listIndex].noCompleted = this.userCards.cards[listIndex].noCompleted.filter(id => id !== taskId);
    this.userCards.cards[listIndex].important = this.userCards.cards[listIndex].important.filter(id => id !== taskId);
    this.userCards.cards[listIndex].items = this.userCards.cards[listIndex].items.filter(task => task.id !== taskId);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }

  setImportantItem(cardId: number, taskId: number, state: boolean) {
    const listIndex = this.indexService.getIndex(this.userCards.cards, cardId);
    this.userCards.cards[listIndex].items[this.indexService.getIndex(this.userCards.cards[listIndex].items, taskId)].important = state;
    state ? this.userCards.cards[listIndex].important.push(taskId) :
      this.userCards.cards[listIndex].important = this.userCards.cards[listIndex].important.filter(id => id !== taskId);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }

  setCompleteItem(cardId: number, taskId: number, state: boolean) {
    const listIndex = this.indexService.getIndex(this.userCards.cards, cardId);
    this.userCards.cards[listIndex].items[this.indexService.getIndex(this.userCards.cards[listIndex].items, taskId)].completed = state;
    state ? this.userCards.cards[listIndex].noCompleted = this.userCards.cards[listIndex].noCompleted.filter(id => id !== taskId) :
      this.userCards.cards[listIndex].noCompleted.push(taskId);
    localStorage.setItem(this.userId, JSON.stringify(this.userCards));
    this.updateNavigationService.change.next(this.userCards.cards);
    return of('ok');
  }
}
