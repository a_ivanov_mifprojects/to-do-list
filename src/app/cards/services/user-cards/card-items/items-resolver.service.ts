import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {CardItemsService} from './card-items.service';

@Injectable({
  providedIn: 'root'
})
export class ItemsResolverService implements Resolve<any> {

  constructor(private cardItemsService: CardItemsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.cardItemsService.getItems(Number(route.params.id));
  }
}
