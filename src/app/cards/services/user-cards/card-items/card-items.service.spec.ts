import { TestBed } from '@angular/core/testing';

import { CardItemsService } from './card-items.service';

describe('CardItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CardItemsService = TestBed.get(CardItemsService);
    expect(service).toBeTruthy();
  });
});
