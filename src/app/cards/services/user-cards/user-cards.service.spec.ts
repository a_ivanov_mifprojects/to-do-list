import { TestBed } from '@angular/core/testing';

import { UserCardsService } from './user-cards.service';

describe('UserCardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserCardsService = TestBed.get(UserCardsService);
    expect(service).toBeTruthy();
  });
});
