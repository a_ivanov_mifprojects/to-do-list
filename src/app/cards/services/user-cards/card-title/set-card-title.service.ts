import {Injectable} from '@angular/core';
import {Card} from '../user-cards-interface';

@Injectable({
  providedIn: 'root'
})
export class SetCardTitleService {

  checkTitle(cards: Card[], title: string) {
    const sameTitles = this.getSameTitle(cards, title);
    if (sameTitles.length > 0) {
      for (let i = 1; i <= sameTitles.length; i++) {
        if (sameTitles.some(list => list.title === `${title} (${i})`)) {
          continue;
        }
        return `${title} (${i})`;
      }
    }
    return title;
  }

  getSameTitle(cards: Card[], title: string) {
    return cards.filter(card => card.title.replace(/\s\(\d+\)+$/gim, '') === title);
  }
}
