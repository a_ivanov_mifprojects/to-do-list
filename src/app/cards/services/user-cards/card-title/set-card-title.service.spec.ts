import { TestBed } from '@angular/core/testing';

import { SetCardTitleService } from './set-card-title.service';

describe('SetCardTitleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetCardTitleService = TestBed.get(SetCardTitleService);
    expect(service).toBeTruthy();
  });
});
