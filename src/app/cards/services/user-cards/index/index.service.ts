import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IndexService {

  getIndex(array: any[], id: number) {
    return array.findIndex(item => item.id === id);
  }
}
