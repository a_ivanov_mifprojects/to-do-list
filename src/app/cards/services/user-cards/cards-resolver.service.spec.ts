import { TestBed } from '@angular/core/testing';

import { CardsResolverService } from './cards-resolver.service';

describe('CardsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CardsResolverService = TestBed.get(CardsResolverService);
    expect(service).toBeTruthy();
  });
});
