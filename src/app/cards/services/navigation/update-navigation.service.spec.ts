import { TestBed } from '@angular/core/testing';

import { UpdateNavigationService } from './update-navigation.service';

describe('UpdateNavigationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateNavigationService = TestBed.get(UpdateNavigationService);
    expect(service).toBeTruthy();
  });
});
