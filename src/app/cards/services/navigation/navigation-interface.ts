export interface Navigation {
  title: string;
  noCompleted: number;
  important: number;
  id: number;
}
