import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CardsResolverService} from './services/user-cards/cards-resolver.service';
import {StatisticsComponent} from './card/statistics/statistics.component';
import {CardComponent} from './card/card.component';
import {ItemsResolverService} from './services/user-cards/card-items/items-resolver.service';
import {CardsComponent} from './cards/cards.component';

const routes: Routes = [
  {
    path: '',
    component: CardsComponent,
    resolve: {
      cards: CardsResolverService
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'important'
      },
      {
        path: 'important',
        component: StatisticsComponent,
        resolve: {
          cards: CardsResolverService
        },
        runGuardsAndResolvers: 'always'
      },
      {
        path: 'noCompleted',
        component: StatisticsComponent,
        resolve: {
          cards: CardsResolverService
        },
        runGuardsAndResolvers: 'always'
      },
      {
        path: ':id',
        component: CardComponent,
        resolve: {
          card: ItemsResolverService
        },
        runGuardsAndResolvers: 'always'
      }
    ],
    runGuardsAndResolvers: 'always'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
